USE DbPratice

GO

CREATE PROC ABPLANGUAGETEXTS_Create 'CarType', 'en', N'Car type'
@key nvarchar(256),
@languageName nvarchar(10),
@value nvarchar(Max) = NULL
as
begin tran
begin try
if(exists (select * from [AbpLanguageTexts] WHERE [Key] = @key and LanguageName = @languageName))
begin
	UPDATE [AbpLanguageTexts]
	SET 
	[Key] = @key,
	LanguageName = @languageName,
	Value = @value
	WHERE [Key] = @key and LanguageName = @languageName
end
else
begin
	INSERT INTO [dbo].[AbpLanguageTexts]
          ([CreationTime]
          ,[CreatorUserId]
          ,[Key]
          ,[LanguageName]
          ,[Source]
          ,[Value])
      VALUES(
        '1999-02-24',
        1,
        @key,
        @languageName,
        'AbpZeroTemplate',
		@value
      )
end
	  commit tran
end try
begin catch
	rollback tran
end catch