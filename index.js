const fs = require("fs");
const data = require("./data.json").data;

function main() {
  try {
    console.log("====== BEGIN =====");
    fs.writeFile("./multi-languages.sql", "", (err) => {
      if (err) console.log("clear file err: ", err);
    });
    fs.appendFile("./multi-languages.sql", "USE DBPratice\nGO\n", (err) => {
      if (err) console.log("user db err: ", err);
    });
    const dataLength = data.length;
    for (let i = 0; i < dataLength; i++) {
      let s;
      let key = data[i].key;
      let value = data[i].value;
      let lang = data[i].languageName;
      s = `ABPLANGUAGETEXTS_Create N'${key}', N'${lang}', N'${value}'\nGO\n`;
      fs.appendFile("./multi-languages.sql", s, (err) => {
        if (err) console.log("err: ", err);
      });
      console.log(
        `INSERT OR UPDATE key = ${key}, value = ${value}, language = ${lang}`
      );
    }
    console.log("===== END =====");
  } catch (err) {
    console.log("Make sql file fail: ", err);
  }
}

main();
